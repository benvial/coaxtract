.. coaxtract documentation master file

=========
coaxtract
=========

**Tools for extracting material properties from open-ended coaxial probe measurements.** 


Welcome! This is the documentation for coaxtract version |release|, last updated on |today|.



.. toctree::
   :maxdepth: 2
   :caption: Quickstart
   
   quickstart
   
   
.. toctree::
   :maxdepth: 2
   :caption: Examples

   auto_examples/index

   
   
.. toctree::
  :maxdepth: 2
  :caption: API reference:
  :hidden:
  
  source/modules
  idx


  
.. toctree::
 :maxdepth: 2
 :caption: Bibliography:
 :hidden:
 
 biblio
  


image: continuumio/miniconda3:latest

# Change pip's cache directory to be inside the project directory since we can
# only cache local items.
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  TERM: xterm

# Pip's cache doesn't store the python packages
# https://pip.pypa.io/en/stable/reference/pip_install/#caching
#
# If you want to also cache the installed packages, you have to install
# them in a virtualenv and cache it as well.
cache:
  paths:
    - .cache/pip
    - .cache/conda

.initialize: &initialize 
    - apt-get update -q && apt-get install -q -y --no-install-recommends make cmake
    - conda update -n base -c defaults conda
    - conda config --set always_yes yes --set changeps1 no
    - conda config --add channels conda-forge
    - conda create --prefix .cache/conda/coaxtract
    - source activate .cache/conda/coaxtract
    - conda install python=3.9
    - python -V  # Print out python version for debugging
    - make install

badge:
  script:
    # Build json info for badges
    - echo "collecting stats for badges"
    - commits=`git rev-list --all --count`
    - latest_release_tag=$(git describe --tags `git rev-list --tags --max-count=1`)
    - logo=$(cat ./doc/_static/_assets/coaxtract.svg | tr '"' "'")
    - echo {\"schemaVersion\":"1", \"commits\":\"$commits\", \"release_tag\":\"$latest_release_tag\"} > badges.json
    - echo {\"schemaVersion\":"1", \"logoSvg\":\"$logo\", \"label\":\"release\", \"message\":\"$latest_release_tag\", \"color\":\"278e75\", \"labelColor\":\"dedede\", \"style\":\"for-the-badge\"} > logobadge.json
  artifacts:
    paths:
        - badges.json
        - logobadge.json
  only:
    - main
    - tags 

  
pytest:
  stage: test
  script:
    - *initialize 
    # Install dev/test requirements
    - make dev
    # Run the test suite
    - make test
  artifacts:
    reports:
      cobertura: coverage.xml
    paths:
      - htmlcov

pages:
  stage: deploy
  dependencies:
    - pytest
  script:
    - *initialize 
    - apt-get update -q && apt-get install -q -y --no-install-recommends wget unzip fontconfig libfontconfig
    # Install fonts
    - mkdir -p ~/.fonts/
    - mkdir -p fonts/
    - cd fonts
    - wget -O JetBrainsMono.zip https://fonts.google.com/download?family=JetBrains+Mono
    - unzip JetBrainsMono.zip && rm -rf JetBrainsMono.zip && rm -rf *.txt
    - mv *.ttf ~/.fonts/ && mv static/*.ttf ~/.fonts/
    - wget -O KulimPark.zip https://fonts.google.com/download?family=Kulim+Park
    - unzip KulimPark.zip && rm -rf KulimPark.zip && rm -rf *.txt
    - mv *.ttf ~/.fonts/
    - cd .. && rm -rf fonts
    - fc-cache -f -v
    - fc-list | grep JetBrainsMono
    - fc-list | grep KulimPark
    # Install doc requirements
    - make doc-req
    # Build documentation html
    - make doc
    - mv htmlcov/ doc/_build/html/coverage
    - mv doc/_build/html/ public/
  artifacts:
    paths:
      - public
  only:
    - main
    - tags

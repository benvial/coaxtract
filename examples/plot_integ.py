#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: MIT


"""
Integration methods
===================

Aperture Admittance calculation.
"""


import matplotlib.pyplot as plt
import numpy as np
from jax import jit
from matplotlib.ticker import MaxNLocator

import coaxtract as cx

timer = cx.Timer()

plt.style.use("../doc/coaxtract.mplstyle")


###############################################################################
# Checking results from :cite:p:`Zhou2017`.


probe = cx.CoaxProbe(epsilon=2.08 * (1 - 0.0006j), rin=0.52e-3, rout=1.2e-3)


def calc(freqs, epsilon, thickness, path="complex", n_int=100):
    layer = cx.Layer(epsilon=epsilon, thickness=thickness)
    stack = cx.Stack([layer], termination="PEC")
    setup = cx.Setup(stack, probe)
    y = setup.admittance(
        freqs,
        path=path,
        alpha=0.5,
        int_method="trapz",
        n_int=n_int,
    )
    return y


calc_jit = jit(calc, static_argnums=(3, 4))


def compute_admittance(
    epsilon, thickness=2e-3, acc=1e-6, n_int=100, maxiter=15, path="complex", freqs=10e9
):

    nconv = 0
    err = 2 * acc
    y = 1
    conv = []
    while np.mean(err) > acc:

        y = calc_jit(freqs, epsilon, thickness, path, n_int)

        if nconv > 0:
            err = np.mean(abs(y - yold))
            conv.append(err)
        nconv += 1
        yold = y
        n_int *= 2
        if nconv > maxiter:
            break

    return y, err, nconv, conv


timer.start()
calc(10e9, 11 - 0.1 * 1j, 1e-3)
timer.stop()
timer.start()
calc(11e9, 5 - 0.8 * 1j, 3e-3)
timer.stop()

admittance, admittance_realint, loss_tan = [], [], []
convergence, convergence_realint = [], []
epsr1 = 2.08
for j in range(-7, 2):
    tand = 10 ** (j)
    print("loss tangent = ", tand)
    epsilon = epsr1 * (1 - 1j * tand)
    y, err, nconv, conv = compute_admittance(epsilon)
    print("complex integration path")
    print("admittance:", y)
    print("error:", err)
    print("number of refinement steps:", nconv)
    admittance.append(y)
    convergence.append(conv)
    y, err, nconv, conv = compute_admittance(epsilon, path="real")
    print("real integration path")
    print("admittance:", y)
    print("error:", err)
    print("number of refinement steps:", nconv)
    print("------------")
    admittance_realint.append(y)
    convergence_realint.append(conv)
    loss_tan.append(tand)

nmax = 5
admittance = np.array(admittance)[0:nmax]
admittance_realint = np.array(admittance_realint)[0:nmax]
loss_tan = np.array(loss_tan)[0:nmax]

fig, ax = plt.subplots(2, 1, figsize=(3, 4))
ax[0].plot(loss_tan, admittance.real, "s-", color="#488a62", label="complex")
ax[1].plot(loss_tan, admittance.imag, "s-", color="#e29c49", label="complex")
ax[0].plot(loss_tan, admittance_realint.real, "o--", color="#488a62", label="real")
ax[1].plot(loss_tan, admittance_realint.imag, "o--", color="#e29c49", label="real")
ax[0].set_ylabel("Re")
ax[1].set_ylabel("Im")
for a in ax:
    a.set_xlabel("loss tangent")
    a.set_xscale("log")
    a.legend()
plt.tight_layout()
plt.show()

#
# ###############################################################################
# # Plot convergence for the first case
#
fig, ax = plt.subplots(1, 1, figsize=(2, 2))
ax.plot(convergence[0], "s-", color="#488a62", label="complex")
ax.plot(convergence_realint[0], "o--", color="#e29c49", label="real")
ax.set_ylabel("error")
ax.set_xlabel("iteration")
# ax.set_xscale("log")
ax.set_yscale("log")
ax.xaxis.set_major_locator(MaxNLocator(integer=True))
ax.legend()
plt.tight_layout()
plt.show()


convergence, convergence_realint = [], []
accuracy = []
epsr1 = 2.08
for j in range(4, 8):
    tand = 1e-4
    acc = 10 ** (-j)
    accuracy.append(acc)
    print("loss tangent = ", tand)
    epsilon = epsr1 * (1 - 1j * tand)
    y, err, nconv, conv = compute_admittance(epsilon, acc=acc, maxiter=20)
    print("complex integration path")
    print("admittance:", y)
    print("error:", err)
    print("number of refinement steps:", nconv)
    convergence.append(nconv)
    y, err, nconv, conv = compute_admittance(epsilon, path="real", acc=acc, maxiter=20)
    print("real integration path")
    print("admittance:", y)
    print("error:", err)
    print("number of refinement steps:", nconv)
    print("------------")
    convergence_realint.append(nconv)


###############################################################################
# Plot convergence

fig, ax = plt.subplots(1, 1, figsize=(2, 2))
ax.plot(accuracy, convergence, "s-", color="#488a62", label="complex")
ax.plot(accuracy, convergence_realint, "o--", color="#e29c49", label="real")
ax.set_ylabel("number of refinement steps")
ax.set_xlabel("desired accuracy")
ax.set_xscale("log")
# ax.set_yscale("log")
ax.legend()
plt.tight_layout()
plt.show()


###############################################################################
# Plot reflection spectra for various materials

freqs = np.linspace(1, 40, 40) * 1e9

R = []
Rreal = []
for epsilon in [1.0 * (1 - 0.0001j), 2.08 * (1 - 0.0006j), 3.82 * (1 - 0.0004j)]:
    timer.start()
    y, err, nconv, conv = compute_admittance(
        epsilon, thickness=1e-3, acc=1e-4, maxiter=20, freqs=freqs
    )
    t = timer.stop()
    r = (1 - y) / (1 + y)
    R.append(r)
    timer.start()
    y, err, nconv, conv = compute_admittance(
        epsilon, thickness=1e-3, path="real", acc=1e-4, maxiter=20, freqs=freqs
    )
    t = timer.stop()
    r = (1 - y) / (1 + y)
    Rreal.append(r)

for r, rreal, case in zip(R, Rreal, ["Air", "Teflon", "Quartz"]):

    fig, ax1 = plt.subplots(1, 1, figsize=(2.5, 2))
    ax1.plot(
        freqs * 1e-9,
        abs(rreal),
        "o",
        color="#488a62",
        label="amplitude (real)",
        alpha=0.5,
    )
    ax1.plot(freqs * 1e-9, abs(r), "-", color="#488a62", label="amplitude (complex)")
    ax1.set_ylabel("amplitude", color="#488a62")
    ax1.set_xlabel("frequency (GHz)")
    # ax.set_yscale("log")
    if case == "Air":
        ax1.set_ylim(0.5, 1.03)
    else:
        ax1.set_ylim(0, 1.03)

    ax1.tick_params(axis="y", labelcolor="#488a62")

    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

    ax2.plot(
        freqs * 1e-9,
        np.angle(rreal) * 180 / cx.pi,
        "o",
        color="#e29c49",
        label="phase (real)",
        alpha=0.5,
    )
    ax2.plot(
        freqs * 1e-9,
        np.angle(r) * 180 / cx.pi,
        "-",
        color="#e29c49",
        label="phase (complex)",
    )
    ax2.set_ylabel("phase (degrees)", color="#e29c49")
    ax2.tick_params(axis="y", labelcolor="#e29c49")

    ax2.set_ylim(-180, 180)
    plt.suptitle(case)

    ax1.legend(loc=3)
    ax2.legend(loc=4)
    plt.tight_layout()
    plt.show()

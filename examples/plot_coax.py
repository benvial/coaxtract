#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: MIT


"""
Basic calculation
=================

Tutorial.
"""


import coaxtract as cx

###############################################################################
# Define layers


film = cx.Layer(epsilon=100 - 0.1j, thickness=10e-6)
sublayer = cx.Layer(epsilon=11 - 0.01j, thickness=1e-3)
substrate = cx.Layer(epsilon=11 - 0.1j, thickness=0.5e-3)

###############################################################################
# Make stack from a list of layers (from top to bottom)

layers = [film, sublayer, substrate]
stack = cx.Stack(layers, termination="PEC")

###############################################################################
# Define the probe

probe = cx.CoaxProbe(rin=0.5e-3, rout=3e-3, epsilon=2.1 * (1 - 0.0004j))

###############################################################################
# Define the setup

setup = cx.Setup(stack, probe)

###############################################################################
# Compute the reflection coefficient

r = setup.reflection(freq=10e9)
print(r)

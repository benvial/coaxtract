#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: MIT
from scipy.constants import c, epsilon_0, mu_0, pi

Z0 = (mu_0 / epsilon_0) ** 0.5

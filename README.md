

[![](https://img.shields.io/endpoint?url=https://gitlab.com/benvial/coaxtract/-/jobs/artifacts/main/file/logobadge.json?job=badge)](https://gitlab.com/benvial/coaxtract/-/releases)
[![](https://img.shields.io/gitlab/pipeline-status/benvial/coaxtract?branch=main&logo=gitlab&labelColor=dddddd&logoColor=ffffff&style=for-the-badge)](https://gitlab.com/benvial/coaxtract/commits/main)
[![](https://img.shields.io/gitlab/coverage/benvial/coaxtract/main?logo=python&logoColor=e9d672&style=for-the-badge)](https://benvial.gitlab.io/coaxtract/coverage)
[![](https://img.shields.io/badge/code%20style-black-dedede.svg?logo=python&logoColor=e9d672&style=for-the-badge)](https://black.readthedocs.io/en/stable/)
[![](https://img.shields.io/badge/license-GPLv3-blue?color=439cb0&logo=open-access&logoColor=white&style=for-the-badge)](https://gitlab.com/benvial/coaxtract/-/blob/main/LICENSE.txt)


# coaxtract

Permittivity extraction for multilayer stacks using an open-ended coaxial probe.

See the documentation at [benvial.gitlab.io/coaxtract](https://benvial.gitlab.io/coaxtract).
